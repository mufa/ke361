<?php
namespace Admin\Controller;
use Admin\Model\CategoryModel;
use TopSDK\Api\TopApi;
use Admin\Model\CollectGoodsModel;
use Admin\Model\GoodsModel;
class CollectController extends AdminController
{
    
    public function index(){
        if(IS_POST){
            $q              =   I('q');
            $cid            =   I('cid');
            $sort           =   I('sort');
            $startTkRate    =   I("start_tk_rate");
            $endTkRate      =   I("end_tk_rate");
            $startPrice     =   I('start_price');
            $endPrice       =   I('end_price');
  
            $pageSize       =   I('num')? I('num') : 30;
            $cateId         =   I('cate_id') ? intval(I('cate_id')) : 0;
            $taobao = new TopApi(C('APP_KEY'), C('APP_SECRET'));
            $result = $taobao->getItemList($q,$cid,false,$startPrice,$endPrice,$startTkRate,$endTkRate,$sort,1,$pageSize);
         
            $CollectGoodsModel = new CollectGoodsModel();
            if(is_array($result)){
                foreach ($result as $k => $v){
                    $result[$k]['cate_id'] = $cateId;
                    if($CollectGoodsModel->create($result[$k])){
                        $result[$k]['id'] = $CollectGoodsModel->add();
                    }                   
                }
                $this->assign('_list',$result);
                $this->display('collect');
            }else{
                $this->error($taobao->error());
            }
        }else{
            $category = D('CategoryGoods')->getGoodsCategory();
             
            $this->assign('category', $category);
            $this->display();
        } 
    }
    public function collectList(){
        $CollectGoodsModel = new CollectGoodsModel();
        $list = $this->lists($CollectGoodsModel);
        $this->assign('_list',$list);
        $this->display('collect');
    }
    public function addToGoods(){
         $id = array_unique((array)I('id',0));       
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }
        $where['id'] =   array('in',$id);
        $CollectGoodsModel = new CollectGoodsModel();
        $info = $this->lists('CollectGoods',$where);
        $GoodsModel = new GoodsModel();
		$add = 0;
		$err = 0;
		$exist = 0;
		foreach($info as $row){
			if($GoodsModel->create($row)){
				$res = $GoodsModel->addGoods();
				if($res>0){
					$where = array(
						'id' => $row['id']
					);
                $CollectGoodsModel->where($where)->delete();
					$add++;
				}elseif($res == -2){
					$exist++;
				}
				else{
					$err++;
				}
            }else {
				$err++;
			}
            }
		if($err > 0){
			$this->error($err.'件商品上架失败，'.$add.'件商品上架成功',U('Collect/collect'));
        }else {
			$this->success($add.'件商品上架成功,'.$exist.'件重复商品未添加',U('Collect/index'));
        }  
    }
    public function update(){
        if(IS_POST){
            $this->display();
            $catId = I('cate_id');
            $where['status'] = 1;
            if(isset($catId)){
                $where['cate_id'] = $catId;
            }
            $GoodsModel = new GoodsModel();
            
            $taobao = new TopApi(C('APP_KEY'), C('APP_SECRET'));
            $err=0;
            $count=0;
            $goodsList = $GoodsModel->field('id,num_iid,title')->where($where)->select();
            if(is_array($goodsList)){
                foreach ($goodsList as $row){
                    if(!empty($row['num_iid'])){
                        $result = $taobao->getItemInfo($row['num_iid']);
                        if($result){
                            foreach ($result as $r){
                                $where['id'] = $row['id'];
                                unset($r['title']);
                                unset($r['pic_url']);
                                $GoodsModel->create($r);
                                $res=$GoodsModel->where($where)->save();
                                if($res){
                                    show_msg('正在更新...'.$row['title']);
                                    $count++;
                                }else{
                                    $where['id'] = $row['id'];
                                    $GoodsModel->where($where)->save(array('status'=>3));
                                    show_msg('更新...'.$row['title'].'...'.$taobao->error());
                                    $err++;
                                }
                            }
                            
                        }else{                      
                            show_msg('更新...'.$row['title'].'...非淘宝天猫商品，跳过'); 
                        }
                    }                                  
                }
                if($err > 0 ){
                    show_msg($err.'件商品更新失败');
                }else {
                    show_msg('全部更新完成');
                }
               ;
            }
            
           
        }else{
            $category = D('CategoryGoods')->getGoodsCategory();
             
            $this->assign('category', $category);
            $this->display();
        }
        
    }
    public function invalidGoods(){
        
    }
    public function del(){
        $id = array_unique((array)I('id',0));       
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }
        $where['id'] =   array('in',$id);
        
       
        if(M('CollectGoods')->where($where)->delete()){
            $this->success('操作成功',U('Collect/index'));
        }else {
            $this->error('删除失败',U('Collect/collectList'));
        }
    }
    
    
}

?>