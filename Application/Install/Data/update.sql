INSERT INTO `ke_menu` VALUES ('149', '专题商品', '131', '0', 'Topic/topicGoods', '1', '', '专题', '0', '1');
ALTER TABLE `ke_goods` CHANGE price market_price decimal(11,2) unsigned not null default 0.00;
ALTER TABLE `ke_goods` CHANGE discount_price price decimal(11,2) unsigned not null default 0.00;
ALTER TABLE `ke_collect_goods` CHANGE price market_price decimal(11,2) unsigned not null default 0.00;
ALTER TABLE `ke_collect_goods` CHANGE discount_price price decimal(11,2) unsigned not null default 0.00;